%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')
%global debug_package %{nil}

Summary: A unified analytics engine for large-scale data processing.
Name: spark
Version: 3.5.0
Release: 5
License: Apache 2.0
URL: http://spark.apache.org/
Source0: https://github.com/apache/spark/archive/v%{version}.tar.gz
Source1: https://github.com/protocolbuffers/protobuf/archive/refs/tags/v3.21.7.tar.gz
Source2: https://github.com/os72/protoc-jar/archive/refs/tags/v3.11.4.tar.gz
Source3: https://github.com/os72/protoc-jar-maven-plugin/archive/refs/tags/v3.11.4.zip
Source4: https://services.gradle.org/distributions/gradle-7.6-bin.zip
Source5: https://github.com/google/protobuf/releases/download/v21.7/protobuf-all-21.7.tar.gz
Source6: https://github.com/grpc/grpc-java/archive/refs/tags/v1.56.0.tar.gz
Patch0001: 0001-change-mvn-scalafmt.patch
Patch0002: 0002-Upgrade-os-maven-plugin-to-1.7.1.patch
Patch0003: backport-CVE-2024-23945.patch

%ifarch riscv64
BuildRequires:  protobuf-devel protobuf-compiler
BuildRequires:  autoconf automake libtool pkgconfig zlib-devel libstdc++-static gcc-c++
Patch1000:      1000-Added-support-for-building-the-riscv64-protoc-binari.patch
Patch1001:      1001-Add-protoc-java-support-for-riscv64.patch
Patch1002:      1002-Added-support-for-building-the-riscv64-protoc-gen-gr.patch
%endif
BuildRequires: java-1.8.0-openjdk-devel git maven

Requires: java-1.8.0-openjdk

ExclusiveArch: x86_64 aarch64 ppc64le loongarch64 riscv64

%description
Apache Spark achieves high performance for both batch and streaming data, using a state-of-the-art DAG scheduler, a query optimizer, and a physical execution engine.

%prep
%ifarch riscv64
mkdir -p ${HOME}/%{name}-prep_dir
export PROTOC_VERSION="3."$(protoc --version | awk '{print $NF}')
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=${PROTOC_VERSION} -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=/usr/bin/protoc
# protoc
tar -mxf %{SOURCE1} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/protobuf-3.21.7
%patch1000 -p1
./autogen.sh
./protoc-artifacts/build-protoc.sh linux riscv64 protoc
mvn install:install-file -DgroupId=com.google.protobuf -DartifactId=protoc -Dversion=3.21.7 -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=protoc-artifacts/target/linux/riscv64/protoc.exe
popd
# protoc-jar
tar -mxf %{SOURCE2} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/protoc-jar-3.11.4
%patch1001 -p1
mvn clean install -Dmaven.test.skip=true -Dmaven.javadoc.skip=true
popd
# protoc-jar-maven-plugin
unzip %{SOURCE3} -d ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/protoc-jar-maven-plugin-3.11.4
mvn clean install -Dmaven.test.skip=true -Dmaven.javadoc.skip=true
popd
# prepare gradle and protobuf for build protoc-gen-grpc-java
mkdir -p %{_tmppath}/source
cp %{SOURCE4} %{_tmppath}/source
tar xzf %{SOURCE5} -C %{_tmppath}/source
# protoc-gen-grpc-java
tar -mxf %{SOURCE6} -C ${HOME}/%{name}-prep_dir
pushd ${HOME}/%{name}-prep_dir/grpc-java-1.56.0
%patch1002 -p1
sed -i "s,@HOME@,${HOME},g" build.gradle
sed -i 's|https\\://services.gradle.org/distributions|file://%{_tmppath}/source|g' gradle/wrapper/gradle-wrapper.properties
SKIP_TESTS=true ARCH=riscv64 ./buildscripts/kokoro/unix.sh
mvn install:install-file -DgroupId=io.grpc -DartifactId=protoc-gen-grpc-java -Dversion=1.56.0 -Dclassifier=linux-riscv64 -Dpackaging=exe -Dfile=mvn-artifacts/io/grpc/protoc-gen-grpc-java/1.56.0/protoc-gen-grpc-java-1.56.0-linux-riscv64.exe
popd
%endif

%setup -q

%patch0001 -p1
%patch0002 -p1
%patch0003 -p1

%ifarch riscv64
sed -i -e 's/protoVersion = "3.23.4/protoVersion = "'${PROTOC_VERSION}/'' project/SparkBuild.scala
sed -i -e 's/<protobuf.version>3.23.4/<protobuf.version>'${PROTOC_VERSION}/'' pom.xml
%endif

%build
%ifarch riscv64
export MAVEN_OPTS="-Xms2048M -Xmx8000M"
%endif
./dev/make-distribution.sh --mvn mvn --name spark-3.5.0 --tgz -DskipTests -Dmaven.test.skip=true

%install
mkdir -p %{buildroot}/opt/
cp -rf ../%{name}-%{version} %{buildroot}/opt/apache-%{name}-%{version}

%files
/opt/apache-%{name}-%{version}


%changelog
* Thu Dec 26 2024 changtao <changtao@kylinos.cn> - 3.5.0-5
- Type: CVE
- CVE: CVE-2024-23945
- SUG: NA
- DESC: fix CVE-2024-23945

* Mon Jul 1 2024 Dingli Zhang <dingli@iscas.ac.cn> - 3.5.0-4
- Add riscv64 to ExclusiveArch
- Fix build on riscv64
- Upgrade os-maven-plugin to 1.7.1

* Tue May 21 2024 Pengda Dou <doupengda@loongson.cn> - 3.5.0-3
- add loongarch64 to ExclusiveArch

* Tue Mar 12 2024 peng.zou <peng.zou@shingroup.cn> - 3.5.0-2
- add ppc64le support

* Mon Sep 25 2023 xiexing <xiexing4@hisilicon.com> - 3.5.0-1
- update spark version 3.5.0

* Wed Aug 10 2022 xiexing <xiexing4@hisilicon.com> - 3.2.2-1
- update spark version

* Fri Nov 12 2021 Wuzeyiii<wuzeyi1@huawei.com> - 1.2
- Update spark version

* Fri Sep 18 2020 Hubble_Zhu<hubble_zhu@qq.com> - 1.0
- Init package

